import Vue from 'vue'
import App from './App.vue'
import '@/assets/css/style.css'
import VueFormWizard from 'vue-form-wizard'
// Style
import "vue-form-generator/dist/vfg.css"; 
import 'vue-form-wizard/dist/vue-form-wizard.min.css'

Vue.use(VueFormWizard)
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
